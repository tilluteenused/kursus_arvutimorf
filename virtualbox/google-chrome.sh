#!/bin/bash

ubuntu2204install()
{
  # Install Required Packages
  sudo apt install -y \
    software-properties-common \
    apt-transport-https \
    wget \
    ca-certificates \
    gnupg2 \
    ubuntu-keyring

  # Import Google Chrome GPG Key
  sudo wget -O- https://dl.google.com/linux/linux_signing_key.pub \
  | gpg --dearmor \
  | sudo tee /usr/share/keyrings/google-chrome.gpg 

  # Import Google Chrome Repository (arch=amd64)
  echo deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/google-chrome.gpg] http://dl.google.com/linux/chrome/deb/ stable main  \
  | sudo tee /etc/apt/sources.list.d/google-chrome.list

  # Install Google Chrome – Stable
  sudo apt update
  sudo apt -y install google-chrome-stable
  
  # gnome'i lisadega majandamine
  sudo apt -y install chrome-gnome-shell
  sudo apt -y install gnome-shell-extension-prefs
  sudo apt -y install gnome-shell-extension-manager
 
}

ubuntu2204remove()
{
	sudo apt autoremove google-chrome-stable --purge
}

ubuntu2204install


