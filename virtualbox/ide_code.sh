#!/bin/bash

sudo snap install code --classic

# bash
code --install-extension rogalmic.bash-debug

# python
code --install-extension ms-python.python
code --install-extension njpwerner.autodocstring

# markdown
#--sisukord
#code --install-extension huntertran.auto-markdown-toc

# c++
code --install-extension ms-vscode.cpptools
code --install-extension ms-vscode.makefile-tools


# süntaksi ilukuva: twolc, lexc, xfscript
code --install-extension eddieantonio.lexc
code --install-extension echols021.twolc-lang
code --install-extension eddieantonio.xfscript


echo $0 $(date) >> $HOSTNAME.log

