#!/bin/bash

# installitavad teemad

APT_TEEMAD=
APT_TEEMAD="${APT_TEEMAD} numix-gtk-theme"
APT_TEEMAD="${APT_TEEMAD} materia-gtk-theme"
for x in $APT_TEEMAD
do
  echo == $x
  sudo apt install -y $x
done

gsettings set org.gnome.desktop.interface gtk-theme "Materia-light-compact"

sudo apt -y install gnome-tweaks

gnome-tweaks
