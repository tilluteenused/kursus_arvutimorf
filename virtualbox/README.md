# Virtuaalmasinaga majandamine

## Plussid & miinused

Plussid:

* Võite julgelt eksperimenteerida virtuaalarvutiga, 
installida peale maha igusugust tarkvara, eksperimenteerida erinevate seadistusega jms
riskeerimata oma "päris" arvuti vussi keeramisega.
Vajadusel saate algse versiooni tagasi võtta või salvestada vahepealseid versioone.
* Kui virtuaalarvutit (või seal olevat tarkvara) enam vaja ei lähe,
on seda lihtne kustutada.
* Teie "pärisarvuti" ja virtuaalarvuti opsüsteemid, programmid ja failid on samaegselt kasutatavad.
* WSList rohkem võimalusi seadistamiseks, virtuaalmasinate salvestamiseks jms.
* Virtualboxis tehtud virtuaalarvutit on lihtne teise arvutisse ümbertõsta.
* Skaleerub (op mälu hulk, protsessrite arv jms) suures ulatuses.
* Kasutuskogemus on üsna sarnane "päris" linuxi omale, sest töötavad graafikakiirendit kasutavad aknahaldurid (näit GNOME3)

Miinused:

* Teie arvutis peab olema "piisavalt" mälu ja kõvaketast päris- ja virtuaalarvuti tarvis.
* Virtuaalarvuti on veidi aeglasem (aga mitte hullult)
* WSList tiba aeglasem.

## Virtualbox-i installeerimine

### Linux

Terminaliaknas:

```commandline
sudo apt-get -y install gcc make linux-headers-$(uname -r) dkms
sudo apt-get -y install virtualbox
sudo apt-get -y install-virtualbox-ext-pack
sudo reboot now # Tee arvutile restart
```

### Windows

[Installeerimisjuhend](https://www.virtualbox.org/wiki/Downloads)  tasub läbi lugeda,
aga toimeta alljärgnevate punktide järgi:

Laadi alla ja installi (kusjuures selle tulemusena ilmunud VirtualBox Manageri tervitusaknas pole vaja midagi teha):

1. [VirtualBox 6.1.38 platform packages](https://download.virtualbox.org/virtualbox/6.1.38/VirtualBox-6.1.38-153438-Win.exe) 
  
2. [VirtualBox 6.1.38 Oracle VM VirtualBox Extension Pack](https://download.virtualbox.org/virtualbox/6.1.38/Oracle_VM_VirtualBox_Extension_Pack-6.1.38.vbox-extpack)

3. Tee arvutile restart

## Ubuntu installimine Virtualboxi virtuaalmasinasse

Kui tahad algusest peale Ubuntu ise installida, järgi [juhendit](https://ubuntu.com/tutorials/how-to-run-ubuntu-desktop-on-a-virtual-machine-using-virtualbox#1-overview)

Mõned märkused:

* Virtuaalmasinale eraldatud op-mälu hulka saab (iga) virtuaalmasina stardi eel muuta. Vastavalt sellele, kas füüsiline või virtuaalne masin vajab rohkem mälu. Ideaalis võiks mõlemale jääda vähemalt 8GB. Oma masina ja ülesannete jaoks optimaalse optimaalse variandi saad leida katse-eksituse
meetodil.

* Protsessorite arvu kohta sama jutt. Kui ei tea mida teha, jaga pooled ühele, pooled teisele. Kui king kuskilt pigistama hakkab andke sinna ressursi juurde.

* Kõvaketta (maksimaalset) suurust on hiljem üsna tülikas muuta. Kõvaketta jaoks soovitan võtta ruumi sellise valemiga: 20G + (teie-tööfailide-jaoks-vajalik-ruum) * N, kus N on minimaalselt 1,2.

* Installerimiskeeleks vali eesti.

* Määra õige klaviatuurilaotus. Kui läks valesti, pole hullu, seda saab hiljem muuta.

* Kui läks täitsa pekki, ikka pole hullu. Paari klikkiga saad äpardunud masina kustutada ja uuele katsele minna :)

## Virtuaalmasina importimine

Kui ise nullist Ubuntut installida ei taha võid importida valmis Ubuntu tõmmise.

Op-mälu suuruse ja protsessorite arvu kohta vaata eelmist punkti.

* Tõmba (Google draivist) [ubuntu2204compmorf.ova](https://drive.google.com/drive/folders/1WA2bVAqA6AUec8SCMYOab8iDbEat-zZb?usp=sharing)
* Kontrolli, et fail pole allalaadimise käigus katki läinud (pead saama sama kontrollsumma)
  * Linux

    ```commandline
    md5sum ubuntu2204compmorf.2022.09.27.ova
    ```

    ```text
    8e12f424bb99a312890f5ce825ae8f62  ubuntu2204compmorf.2022.09.27.ova
    ```

  * Windows

    ```commandline
    certutil -hashfile ubuntu2204compmorf.2022.09.27.ova MD5
    ```

    ```text
    MD5 hash of ubuntu2204compmorf.2022.09.27.ova:
    8e12f424bb99a312890f5ce825ae8f62
    CertUtil: -hashfile command completed successfully.
    ```

* Menüüst ```File```  -> ```Import Appliance``` 
* Avanenud dialoogiaknas vali allalaaditud ```ubuntu2204compmorf.20220927.ova``` ja ```Next```
* Avanenud ```Appliance Settings``` aknas vaata üle virtuaalmasinale antava mälu hulk ja ```Import``` (8G virtuaalsele masinale + 8G füüsilisele masinale võiks olla OK).

## (Imporditud) Virtuaalmasina esmakordne kasutamine

* Käivita ```virtualbox```
* Vali virtuaalmasin ja ```Start```
* Kliki ```virtualbox``` ja sisesta parool ```V1rtua1B0x```
* Virtualboxi menüüst: ```Devices```-> ```Insert Quest Additions CD image...``` 
* Terminali aknas (parool ```V1rtua1B0x```)

    ```commandline
    sudo apt-get -y update
    sudo apt-get -y dist-upgrade
    sudo /media/virtuser/VBox_*/VBoxLinuxAdditions.run
    sudo reboot
    ```

* Kliki ```virtualbox``` ja sisesta parool ```V1rtua1B0x```
* Virtualboxi menüüst: ```Devices``` -> ```Optical Drives``` -> ```Remove Disk From virtual drive```
* Virtualboxi menüüst (et töötaks copy-paste füüsilise ja virtuaalmasina vahel):
	```Devices``` -> ```Shared Clipboard``` -> ```* Bidirectional```
	```Devices``` -> ```Drag and Drop``` -> ```* Bidirectional```

## Virtuaalmasinast füüsilise masina kataloogile ligipääsu tekitamine

* Tekita füüsilisse masinasse kataloog nimega ```jagatud_fm```
* Virtualboxi menüüst vali ```Devices``` -> ```Shared Folders```
* Avanenud aknas kliki kataloogi lisamise ikooni (parempoolses ääres)
* Avanenud aknas Folder Path'i vali füüsilisse masinasse tehtud kataloog ja
     ```Folder Name``` lahtrisse kirjuta ```jagatud_fm```
* Sulge dialoogiaknad
* Linuxis ava terminaliaken ja kirjuta sinna:

    ```commandline
     mkdir ~/jagatud
     sudo mount -o uid=1000,gid=1000 -t vboxsf jagatud_fm jagatud
     ```

* Võib virtuaalmasinasse jagada ka mõnda olemasolevat kataloogi füüsilisest 
masinast. Sellisel juhul asendaege eelnevas näites ```jagatud_fm``` oma
katalooginimega.

## Tekstiredaktorid

Kõige lihtsam: ```gedit```, soovitus on ära õppida ```code```.

## Kui lõpetad virtuaalmasinaga töö

***_Ära "tõmba juhet seinast välja"._***

Selle asemel kliki süsteeminenüü Väljalülitamise nuppu (ülemisest parempoolsest nurgast avaneb vastav menüü). 

