# Töökeskkondade võrdlus

## Märkused

* Võrdlus on tehtud 12.10.2022
* Kõigi katsete korral ```cd``` käsuga valisin jooksvaks selle kataloogi kus asuvad testfailid ja programmid ja 
käivitasin kaks korda käsu ```time ./test.sh``` või ```time ./test2.sh```. Tulemused alljärgnevas tabelis.
* 22.04 ISO ja Microsofti poe Linuxid on erinevad.
  * MS Poe versioonis ei tööta systemd deemon. (seega ka snap'i abil installimine jms).
  * Erinevad on glibc versioonid (MS Poe versioonis kompileeritud vmeta ei tööta ISO pealt installitud Ubuntus).
  * Erinevusi on kindlasti rohkem, need torkasid kohe silma.

## Tulemused

| # | keskkond         | failid | time: real              | time: user          | time: sys             |
|---|------------------|--------|-------------------------|---------------------|-----------------------|
| 1 | W11 & wsl2       | linux  | 003m40,190s/003m42,222s | 3m27,562s/3m29,719s | 00m12,620s/00m12,480s |
| 2 | W11 & wsl2       | win    | 040m58,911s/040m06,671s | 9m04,573s/8m29,211s | 08m29,211s/07m47,841s |
| 3 | W11 & virtualbox | linux  | 004m28,710s/004m28,240s | 4m09,467s/4m09,574s | 00m18,737s/00m18,430s |
| 4 | W11 & virtualbox | win    | 116m07,083s/116m12,764s | 7m48,040s/8m31,182s | 48m47,818s/47m4,280s  |
| 5 | linux            | linux  | 003m58,826s/003m54,517s | 3m38,714s/3m34,261s | 00m20,086s/00m19,560s |

## Selgitused tabeli ridade kohta

1. Windows 11 ja WSL2 peale on paigaldatud Microsofti poest Ubuntu 22.04. 
Testprogrammid ja failid paiknevad Linuxi kodukataloogis (```/home/vaino/testfailid```).
2. Windows 11 ja WSL2 peale on paigaldatud Microsofti poest Ubuntu 22.04. 
Testprogrammid ja failid paiknevad Windowsi kodukataloogis (```/mnt/c/User/vaino/My\ Documents/testfailid```).
3. Windows 11 ja VirtualBoxi peale on paigaldatud Ubuntu veebilehelt tõmmatud Ubuntu 22.04 Workstation ISO.
Testprogrammid ja failid paiknevad Linuxi kodukataloogis (```/home/vaino/testfailid```).
4. Windows 11 ja VirtualBoxi peale on paigaldatud Ubuntu veebilehelt tõmmatud Ubuntu 22.04 Workstation ISO.
Testprogrammid ja failid paiknevad Windowsi kodukataloogis (```/mnt/c/User/vaino/My\ Documents/testfailid```)
mis on monteeeritud ```/home/vaino/jagatud``` kataloogi alla.
5. Samasse arvutisse kus jooksis eelmiste katsete Windows 11 on dualboodiga paigaldatud Ubuntu veebilehelt tõmmatud Ubuntu 22.04 Workstation ISO.
Testprogrammid ja failid paiknevad Linuxi kodukataloogis (```/home/vaino/testfailid```).

