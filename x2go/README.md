# X2Go

## Plussid & miinused

Plussid:

* Vajab väga vähe Teie arvutiressursi. Teie arvuti näitab ainult pilti, programmid töötavad teises arvutis.
* Kuna Teie arvutisse saadetav pilt on hästi nutikalt kokkupakitud, töötab talutavalt üle suhteliselt aeglase interneti.
* Kui te panete X2Go akna "ristist" kinni, siis sessioon jääb käima (programmid jäävad tööle) ja kui avate uuesti 
X2Go akna samasse masinasse, saata jätkata sama sessiooniga. Kui pole just pakilist vajadust töö pooleli jätta ja
hiljem jätkata, logige X2Go aknas olevast Linuxist välja, muidu kulutate mõtetult serveri ressursi.

Miinused:

* Teil peab olema X2Go kaudu ligipääs mingile arvutile.
* Teil peab olema X2Go sessiooni töötamise ajal internetiühendus.
* X2Go kliendiga ühilduvad aknahaldurid ajavad asja ära aga ei ole kõige mugavamad ja nägusamad.

## Installeerimine

Installeerige X2Go **_klient_**.
[Installeerimisjuhendid (Windows, Linux jne...)](https://wiki.x2go.org/doku.php/doc:installation:start)

## Kasutamine

Kasutajatunnuse ja parooli saad kursusejuhendaja käest

### Sessiooni seadistamine

* Käivita (töölaualt) ```X2Go Client```
* Avanenud dialoogiaknas: 
    * ```Sessiooni nimi:```  ```AM22```
    * ```Aadress:``` ```193.40.154.115```
    * ```Login:``` TEIE-KASUTAJATUNNUS
    * ```Sessiooni tüüp:```  ```LXDE```
    
### Eelseadistud sessiooni käivitamine

* Käivita (töölaualt) ```X2Go Client```
* Kliki parempoolsel paneelil eelseadistatud sessiooni nimel ```AM22```
* Avanenud dialoogiaknasse pane parool (esimene kord kursusejuhendaja käest saadud parool)

### Esmakordsel sisselogimisel

#### Muuda parool

Kui oled sisseloginud
* kliki all paremas nurgas oleval "pääsukese" ikoonil
* avanenud menüüst vali ```System Tools``` -> ```LXTerminal``` või ```UXTerm```
* avanenud terminaliaknas muuda kursusejuhendaja käest saadud parool endale meelepäraseks ```passwd``` käsu abil.

```cmdline
passwd
```

Vastavalt juhistele tipi esmalt vana kursusejuhendajalt saadud parool ja siis kaks korda oma uus parool.

#### Soovi korral LXDE seadistamine

LXDE on (pea-aegu) kõige resursisäästlikum Linuxi aknahaldur.
Hea kompromiss, kui arvutil vähe mälu ja protsessor nõrk.
Kui tundub, et arvuti ei vea enam hästi Windows'i välja tasub proovida
[Lubuntu't](https://lubuntu.me/jammy-released/) 
