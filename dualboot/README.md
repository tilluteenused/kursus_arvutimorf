# Dualboot

## Plussid ja miinused

Plussid:

* Täisväärtuslik LINUX.
* Mõlema opsüsteemi päralt on kogu arvuti.

Miinused:

* Ei saa olla **_korraga_** mõlemas opsüsteemis (mis kui aus olla on päriselus kaunis tüütu).

## Seadistamine

[Dualboodi (Windows/Ubuntu 22.04) seadistamise juhised](https://www.linuxtechi.com/dual-boot-ubuntu-22-04-and-windows-11/).
Arvasta seda tehes järgmiste asjaoludega:
  * Eelduseks on, et Windows on juba tehases arvutisse installitud.
  * Selleks, et teha dualboodiga arvutit tuleb (suure tõenäosusega) arvuti BIOSis seadeid muuta. Seda tehes:
    * Ärge usaldage suvalist Googlist leitud juhist.
    * BIOSi seadeid laske muuta sellel kes **_päriselt_** teab mida teeb.
    * Pigem jääge Windows 11 + Virtualbox variandi juurde.
  * Ubuntu jaoks soovitan võtta ruumi sellise valemiga: 20G + (teie-tööfailide-jaoks-vajalik-ruum) * **_N_**, kus **_N_** on minimaalselt 1,2.
  * Ubuntu partitsioneerimist ei maksa nii keeruliseks ajada nagu seda ülaltoodud juhendis on tehtud.
  Kõige lihtsam on jätta kogu partitsioneerimine Ubuntu installika teha.
  Minu soovitus oleks teha 2 partitsiooni (siis saate vajadusel Linuxi niimoodi ümberinstallida,
  et uus install kirjutatab üle ainult ```/``` partitsiooni (ehk selle partitsiooni, kus asub op-süsteem) ja ei puutu teie isiklike failidega ```/home``` partitsiooni):
    * 20G ja monteerige see ```/``` alla.
    * Ülejäänud vaba ruum ```/home``` alla.
