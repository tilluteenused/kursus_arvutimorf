#!/bin/sh

# Käsirea parameetrite kuvamise näide
# Kasutusnäide
#  ./parameetrid.sh üks kaks kolm neli viis

echo '$0='$0 # 1. parameeter = programmi nimi
echo '$1='$1 # 2. parameeter = 1. string programmi nime järel
echo '$2='$2 # 3. parameeter = 2. string programmi nime järel
echo '$3='$3 # jne
echo '$*='$* # = $1 $2 ...


