# muutujad

EXTIN=xml
EXTOUT=lause-real
COMMAND=xml_2_lause-real-foorumid.sh

INFILES=$(wildcard *.$(EXTIN))
OUTDIR=$(subst /home/$(USER)/,~/,$(subst /$(EXTIN),/$(EXTOUT),$(CURDIR)))
OUTFILES=$(addprefix $(OUTDIR)/,$(addsuffix .$(EXTOUT), $(basename $(INFILES))))
LOCALBIN=bin
ROOTBIN=../../bin

# reeglid
$(OUTDIR)/%.$(EXTOUT) : %.$(EXTIN)
	( $(LOCALBIN)/$(COMMAND) < $< \
	  > $(@D)/$(@F) || rm -f $(@D)/$(@F) ) 2>> $(@D)/JAMAD.LOG

# käsud
all : $(OUTFILES)
	$(ROOTBIN)/cat-non-empty-file.sh $(OUTDIR)/JAMAD.LOG

make :
	mkdir -p $(OUTDIR)
	cp /dev/null Makefile.lause-real.dep
	cp /dev/null $(OUTDIR)/JAMAD.LOG
	for f in $(INFILES); do echo $(addprefix '$(OUTDIR)'/,$(addsuffix .$(EXTOUT), $${f/.$(EXTIN)/})) : $${f}>> Makefile.lause-real.dep; done

clean :
	-rm -f $(OUTFILES) $(OUTDIR)/JAMAD.LOG

cleanlog :
	cp /dev/null $(OUTDIR)/JAMAD.LOG

catlog :
	$(ROOTBIN)/cat-non-empty-file.sh $(OUTDIR)/JAMAD.LOG

test :
	echo $(subst ', ,$(OUTDIR))
	echo $(OUTDIR)

# sõltuvused
-include Makefile.lause-real.dep

