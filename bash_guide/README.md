# BASH - näpunäiteid

Peamiselt copy-paste ~10 aastat tagasi kirjutatud [PDF-slaididest](https://gitlab.com/tilluteenused/kursus_arvutimorf/-/blob/main/bash_guide/unix-lingvistidele-vol2.pdf). Peamiselt märksõnade tasemel jutt. Jutu vanus eriti ei loe.

## Lühidalt (arvuti)keeltest

Olulisemad keelterühmad:

* Masinkood ja assembler. Sellest saab arvuti "ilma tõlkimata" aru. Lõppude lõpuks tõlgitakse mistahes keeles kirjutatud programm masinkoodiks.
* Programmeerimiskeeled C, C++ (ja tulevikus ehk Rust). Operatsioonisüsteemide, draiverite, suurte programmide (MSO, OO) kirjutamiseks. C-keele väljasuremise põhjused: dünaamilise mäluhalduse ja stringimajanduse puudused.
* Programmeerimiskeel Java. Valdavalt veebirakenduste kirjutamiseks.
* Skriptide kirjutamise keeled: Perl, Python. Regulaaravaldiste töötlusvahendid kaasas. Päris palju kasutatakse 
Perli erinevaid (C, C++, ...) programme kombineeriva kestprogrammina (näit ESTNLTK).
* Skriptide kirjutamise keeled: zsh, bash, sh, ... Võimaldab muuhulgas regulaaravaldisi kasutavaid  käsureaprogramme.
omavahel kombineerida.
* Olelusvõitluse kaotanud keeled: Pascal, PL1, Algol, ...
* Spetsiifilisele ülesannete grupile orienteeritud keeled: Prolog, Fortran, ...

## Väike valik olulisi käsureprogramme

| käsk  | selgitus |
|-------|-----------------------|
| pwd   | jooksva kataloogi nimi |
| rm    | faili kustutatamiseks |
| cd    | töökataloogi vahetamiseks |
| cat   | faili sisu väljundisse (ekraanile) kuvamiseks |
| less  | faili lehekülghaaval edasi-tagasi lappamiseks |
| ls    | kataloogi sisu ekraanile kuvamiseks |
| ls -l | kataloogisisu detailseks kuvamiseks |
| chmod | faili/kataloogi kasutusõiguste määramiseks |

Mõni näide:

```commanline
ls -l
total 16
-rw-rw-r-- 1 tarmo tarmo 3525 Sep 18 13:10 MisSeisOn.txt
-rwxr-x--- 1 tarmo tarmo   25 Sep 18 21:51 proov.sh
```

Faili omaniku, grupi, kõigi ülejäänute õigused.

| Kood | Selgitus | Selgitus|
|---|---|---|
| 4 | **R**ead | õigus lugeda faili, kuvada kataloogi sisu |
| 2 | **W**rite | õigus kirjutada faili, lisada kustutada kataloogist faile ja alamkatalooge |
| 1 | e**X**ecute | *_faili_* puhul annab õiguse käivitada fail programmina, *_kataloogi_* puhul annab õiguse otsida sealt faile-alamkatalooge ja neid kasutada vastavalt sellele millised õigused on failidele-alamkataloogidele antud.

Olulised numbrid ```chmod``` käsu juures: 4+2=6=rw-, 4+2+1=7=rwx, 4+1=5=r-x 

## Sisendi-väljundi ümbersuunamine

Standard sisendfail, standard väljundfail, standard veafail ja selle ümbersuunamine

```käsk  < fail``` standard sisend klaviatuuri asemel failist fail

```käsk  > fail``` standard väljund ekraani asemel faili fail

```käsk 2> fail``` veateated ekraani asemel faili fail

```käsk >> fail``` standard väljund lisatakse faili fail sappa

Nooletaga ümbersuunamist võib kombineerida, näiteks nii:

```käsk < sisendfail > väljundfail 2> veateadetefail```

Eelmise käsu väljundi võib suunata järgmise käsu sisendisse:

```eelmine-käsk | järgmine-käsk```

Näiteks

```cat lähtefail | käsk1 | käsk2 | ... | käskN | less```

```cat lähtefail | käsk1 | käsk2 | ... | käskN > tulemusfail```

```käsk1 < lähtefail | käsk2 | ... | käskN > tulemusfail```

## Skriptifaili esimene rida LINUXis

Skriptifaili peab alustama rida kus on kirjas skripti interpreteeriva programmi nimi.

Näiteks:

```#!/bin/bash```

See ütleb et programm ```/bin/bash``` hakkab tekstifailis olevate käskude täitmist korraldama.

Python'i skripti puhul peaks esimene rida (sõltuvalt olukorrast) olema

```#!/usr/bin/python3```

või

```#!/usr/bin/env python3```

Windowsi see rida ei sega (nagu tavaline kommentaar), windows arvestab failinime laiendiga. 

## Valik eeldefineeritud muutujaid


Väike valik eeldefineeritud muutujatest:

| Muutuja|Selgitus|
|-|-|
|$0|programmi nimi sellisel kujul, nagu ta oli käsureal|
|$1|esimene parameeter|
|$2|teine parameeter|
$N|N'is prameeeter, N on number 0, 1, 2, 3, ...,9 |
|$*|kõik parameetrid alates parameetrist $1|

Vaata näidet [parameetrid.sh](https://gitlab.com/tilluteenused/kursus_arvutimorf/-/blob/main/bash_guide/parameetrid.sh)

## Soovitatavad skriptifaili parameetrid

Iga skriptifailvõiks reageerida sellistele lippudele: 

| Lühike versioon | Pikk versioon | Selgitus        |
|-----------------|---------------|-----------------| 
| -h              | --help        | käsurea süntaks |
| -v              | --version     | versiooni-info- | 

Iga skriptifail peaks sisaldama kommentaari selle kohta:

* mida ta teeb
* millised on kitsendused sisendile
* mida ta välja annab

Vaata näidet [failid-suurtahelisteks.sh](https://gitlab.com/tilluteenused/kursus_arvutimorf/-/blob/main/bash_guide/failid-suurtahelisteks.sh)

## Tsükkel üle failide

### bash'i sissehitatud for tsükkel

Sobib kui failide asukoht failipuus on teada.

Näiteks:

```cmdline
for f in *.sh; do wc -l $f; done
```

Vt skripti [failid-suurtahelisteks.sh](https://gitlab.com/tilluteenused/kursus_arvutimorf/-/blob/main/bash_guide/failid-suurtahelisteks.sh)


### find käsk

Sobib kui failide asukoht failipuus pole teada.

Näiteks:

```cmdline
find . -name '*.sh' -print
```

```cmdline
find . -name '*.sh' -exec wc -l {} \;
```

### make käsk

Sobib kui:

* failide asukoht failipuus on teada, 
* saab töödelda mitut faili sama-aegselt (```make käsu -j N``` parameeter N)
* kui samu faile on korduvalt vaja uuesti töödelda (sest osades lähtefailides toimus mingi 
muutus).


Näiteks:

Lihtne näide [mf_2upper.makefile](https://gitlab.com/tilluteenused/kursus_arvutimorf/-/blob/main/bash_guide/mf_2upper.makefile)

Reaalne näide sellest, kuidas toimub korpuste teisendamine: 
[mf_lause-real.makefile](https://gitlab.com/tilluteenused/kursus_arvutimorf/-/blob/main/bash_guide/mf_lause-real.makefile)

## Veel mõned käsureaprogrammid

### tr

Sobib:
* reavahetuste kaotamiseks/lisamiseks
* suurtähtede teisendamiseks väiketähtedeks, väiketähtede teisendamiseks suurtähtedeks
* (korduvate) tähtede kustumiseks/asendamiseks

Näited:

```cmdline
echo ÜLISUUREDväiksed | tr '[:upper:]' '[:lower:]'
Ülisuuredväiksed
```

```cmdline
echo ÜLISUUREDväiksed | tr '[:lower:]' '[:upper:]'
ÜLISUUREDVäIKSED
```

```cmdline
echo "sona1   sona2     sona3" | tr -s ' '
sona1 sona2 sona3
```

```cmdline
echo terekest | tr 'e' 'E'
tErEkEst
```

```cmdline
echo terekest | tr 'et' 'ET'
TerEkEsT
```

```cmdline
echo terekest | tr -d 'e'
trkst
```

```cmdline
echo terekest | tr -d 'et'
rks
```

Spetsiaalsed sümbolid:

```'\t'``` – tabulatsioon

Windowsi reavahetus koosneb kahest sümbolist ```'\r'``` ja ```'\n'```

Linuxi reavahetus ainult ühest sümbolist ```'\n'```

Käsk ```man tr``` annab lisateavet.


### sed

**_Sobib_** tekstifailist ridades etteantud regulaaravaldisele vastavate asenduste tegemiseks

Parameetrid:

```sed 's/mida-asendada/millega-asendada/g'```

Kaldkriipsude asemel võib kasutada ka mõnda muud eraldajat (näiteks ```#```), soovitatavalt sellist, mida 
mustris ei esine. 
Regulaaravaldistes saab kasutada:

| Metasümbol | Selgitus                                               |
|------------|--------------------------------------------------------|
| .          | (punkt) suvaline sümbol                                |
| ^          | regulaaravaldise esimese sümbolina tähistab rea algust |
| $          | regulaaravaldise viimase sümbolina tähistab rea lõppu  |
| [kptgbd]   | suvaline konsonant| 
| [0-9]  | suvaline number| 
| [^0-9]  | mittenumber number| 
| [0-9]*  | null või mitu numbrit| 
| [0-9][0-9]* | üks või mitu numbrit| 
| \(muster\)  | regulaaravaldiste grupeerimiseks mida-asendada osas| 
| \1 \2 ...   | mida-asendada osas leitud regulaaravaldise kasutamiseks millega-asendada osas |

Käsk ```man sed``` annab lisateavet.


### grep

Sobib tekstifailist etteantud regulaaravaldisele (mitte)vastavate ridade leidmiseks.

Parameetrid:

| Parameeter | Selgitus |
|------------|----------|
| -v         | leiab regulaaravaldist mittesisaldavad read |
| -n         | väljundisse lähevad regulaaravaldist sisaldavad read, iga rea ette lisatakse tema järjekorranumber esialgses failis. |
 
Regulaaravaldistes saab kasutada:

| Metasümbol | Selgitus |
|------------|----------|
| .   | (punkt) suvaline sümbol | 
| ^   | regulaaravaldise esimese sümbolina tähistab rea algust | 
| $   | regulaaravaldise viimase sümbolina tähistab rea lõppu | 
| [kptgbd]   | suvaline konsonant | 
| [0-9]   | suvaline number | 
| [^0-9]  |  mittenumber number | 
| [0-9]*   | null või mitu numbrit | 
| [0-9][0-9]*  | üks või mitu numbrit | 

Käsk ```man grep``` annab lisateavet.
