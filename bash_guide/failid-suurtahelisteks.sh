#!/bin/sh

# Logi:
# 2008.millaski: TV näidiskript, misiganes kursust ma siis andsin
# 2024.11.22: TV Lisasin selle kommentaari

VALJUNDFAILI_LAIEND=upper

# see funktsioon teisendab std-sisendit std-väljundisse
suurtaheliseks()
	{
	tr '[:lower:]' '[:upper:]'
	}

if [ "$1" = "--help" ]
then
	# kasutaja ei tea mida teha, vaja kuvada spikker ekraanile
	echo syntax: 
	echo "   " $0 '[--help] [--version] [fail...]'
	echo "Sisendfail(id) tehakse suurtäheliseks."
	echo "Kui sisend tuli failist, siis väljunfaili nimi saadakse"
	echo "sisendfaili nimest laiendi asendamise teel (.$VALJUNDFAILI_LAIEND)"
	exit 0
elif [ "$1" = "--version" ]
then
	# Kuvame versiooni-infot.
	# Versiooni-infot võib käsitsi sisse kirjutada,
	# aga mõistlikum on lasta seda versioonihaldustarkvaral (CVS, SVN)
	# automaatselt teha 
	echo '$Date: 2008-05-13 17:20:07 +0300 (T, 13 mai 2008) $'
	echo '$Revision: 528 $'
elif [ "$*" = "" ]
then
	# käsurida oli tühi, teisendame std-sisendit std-väljundiks
	suurtaheliseks
else
	# keerame käsurealt etteantud faile suurtäheliseks
	for f in $*
	do
		nimi_ilma_laindita=`echo $f|sed 's#^\(.*\)\.[^./]*$#\1#g'`
		echo === $f '-->' $nimi_ilma_laindita.$VALJUNDFAILI_LAIEND
		suurtaheliseks < $f > $nimi_ilma_laindita.$VALJUNDFAILI_LAIEND
	done
fi


