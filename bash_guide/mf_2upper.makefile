# Tee kõik vajalikud failid
all : proov.upper proov2.upper

# plats puhtaks 
# NB! '-' märk rm käsu ees
clean :
	-rm proov.upper
	-rm proov2.upper

# seoste kirjeldused
proov.upper : proov.txt
	./failid-suurtahelisteks.sh proov.txt

proov2.upper : proov2.txt
	./failid-suurtahelisteks.sh proov2.txt

