#!/bin/bash

# MS CODE IDE

## installime läbi repo&apt'i sest snap ei tööta
sudo apt install -y gnupg2 software-properties-common apt-transport-https curl
curl -sSL https://packages.microsoft.com/keys/microsoft.asc | sudo apt-key add -
sudo add-apt-repository --yes "deb [arch=amd64] https://packages.microsoft.com/repos/vscode stable main"
sudo apt update
sudo apt install -y code

## installime mõned kasulikud laiendused

source ~/.bashrc

### bash
code --install-extension rogalmic.bash-debug

### python
code --install-extension ms-python.python
code --install-extension njpwerner.autodocstring

# süntaksi ilukuva: twolc, lexc, xfscript
code --install-extension eddieantonio.lexc
code --install-extension echols021.twolc-lang
code --install-extension eddieantonio.xfscript


# Et ei tüütaks windoosa versiooni soovitamisega
echo 'export DONT_PROMPT_WSL_INSTALL=defined' >> ~/.bashrc
