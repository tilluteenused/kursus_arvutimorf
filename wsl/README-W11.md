# Windows 11 + Ubuntu22.04@WSL

## Plussid & miinused

Plussid:

* Võite julgelt eksperimenteerida virtuaalarvutiga, 
installida peale maha igusugust tarkvara, eksperimenteerida erinevate seadistusega jms
riskeerimata oma "päris" arvuti vussi keeramisega.
* Kui virtuaalarvutit (või seal olevat tarkvara) enam vaja ei lähe,
on seda lihtne kustutada.
* Teie "pärisarvuti" ja virtuaalarvuti opsüsteemid, programmid ja failid on samaegselt kasutatavad.
* Virtualboxist tiba kiirem.
* Windows 11 korral (erinevalt Windows 10st) graafilise kasutajaliidesega programmid töötavad ilma täiendava pusimiseta.
(Linuxis **_ei pea_** kasutama ainult terminaliakent koos ```nano```-laadse tekstiredaktoriga.)

Miinused:

* Teie arvutis peab olema "piisavalt" mälu ja kõvaketast päris- ja virtuaalarvuti tarvis.
* Virtualboxist vähem võimalusi seadistamiseks, virtuaalmasinate salvestamiseks jms. Või vähemalt keerulisem.

## Windowsi WSI-i seadistamine

### Eeltöö Windowsis 

* Windowsi seadistamine
    * Uuenda Windowsi tarkvara ja **_taaskäivita_** arvuti.
    * WSLi lubamine
        * Windowsi otsingulahtrisse: ```windowsi funktsioonide sisse- või väljalülitamine``` 
        ja pane linnukesed nende valikute ette
            * [V] Virtuaalplatvorm
            * [V] Windowsi alamsüsteem Linuxile
    * Kui pidid kuskile linnukese panema, **_Taaskäivita arvuti_**, muidu pole vaja.

Edasi toimeta
[Microsofti juhendi](https://learn.microsoft.com/en-us/windows/wsl/tutorials/gui-apps)
järgi. Minu jaoks see toimis.

Kui ei tea, mis graafikadraiver on otsi ```seadmehaldur``` või ```hardware manager``` ja vaata ```kuvaadapter```-i alt.

## Ubuntu installimine

Microsoft Store (otsingusse: ```Ubuntu 22.04```)  Windows-i otsingusse ja sealt edasi juhiste järgi.
Installeerimise käigus küsib Ubuntu, 
millist kasutajanime ja parooli tahad Linuxis kasutada. Võid valida endale meelepärase
kasutajanime ja parooli. Seda parooli küsitakse iga kord kui on vaja ```sudo``` käsu
abil Linuxis administraatori õigusi saada.

## Eeltöö wsl-ubuntu-terminaliaknas

* Käivita Ubuntu wsl-terminaliaken (Windowsi otsingusse: ```ubuntu 22.04```)

* Installime versioonihaldustarkvara ```git```

```commandline
sudo apt install -y git
```

* Tõmbame GITLABist need abimaterjalid oma arvuti kataloogi ```~/git/kursus_arvutimorf.gitlab```

```commandline
mkdir ~/git; cd ~/git
git clone https://gitlab.com/tilluteenused/kursus_arvutimorf.git kursus_arvutimorf.gitlab
cd kursus_arvutimorf.gitlab/wsl/
```

* Uuenda Ubuntu tarkvara (seda peaks vähemalt kord kuus tegema)

```commandline
./uuenda.sh
```

* Installi HFSTga seotud tarkvara. Igaks juhuks koos python'i värgiga, äkki läheb hiljem vaja. 
Kui see on korra tehtud, siis nii ongi, rohkem pole vaja. Kõigi programmide ja tarkvara uuendamine toimub automaagiliselt
igakuise ```sudo apt update && sudo apt -y dist-upgrade``` käsu abil.

**_NB! Alljärgnev käsk töötab, kui installisid Ubuntu 20.04. Kui installisid ```Ubuntu 20.04``` versiooni või lihtsalt ```Ubuntu```
(see installib hetkel samuti ```Ubuntu 20.04``` versiooni), siis paketil ```python3-hfst``` on teine nimi._**

```cmdline
./hfst-ja-python.sh
```

* Installi MS Code-IDE (õigete pluginatega teeb mis vaja, hea kompromiss funktsionaalsuse ja kasutamismugavuse vahel).

```cmdline
./ide_code.sh
```

## Ubuntu virtuaalrvuti käivitamine

Käivita Ubuntu wsl-terminaliaken (Windowsi otsingusse: ```ubuntu 22.04```)

# MS Code-IDE kasutamine

Kui tahad redigeerida ühte konkreetset faili, kirjuta terminaliaknasse:

```cmdline
code FAILINIMI &
```

Kui oled käsurealt kataloogis kuhu oled kokku korjanud ühe portsu faile, millega parajasti tegeled, kirjuta terminaliaknasse:

```cmdline
code . &
```

Punkt tähistab _jooksvat kataloogi_.

Tuleks arvesta, et tegemist _ei ole_ ühekonkreetse asja tegemiseks mõeldud programmiga.
```code``` on platform, mis sõltuvalt installitud plugin'itest on võimeline tegema väga erinevaid asju.

