#/bin/bash

# installime LXDE aknahaldduri
sudo apt install -y lxde lxdm lxsession-logout

# Seadistame linusi selliselt et XRDPga saaks LXDE aknbahgaldurile ligi
sudo apt install xrdp

sudo cp /etc/xrdp/xrdp.ini /etc/xrdp/xrdp.ini.bak
sudo sed -i 's/3389/3390/g' /etc/xrdp/xrdp.ini # So it doesn't interfere with Windows RDP on 3389

sudo cp /etc/xrdp/startwm.sh /etc/xrdp/startwm.sh.bak
sudo sed -i 's_^\(test -x /etc/X11/Xsession && exec /etc/X11/Xsession\)$_#\1_' /etc/xrdp/startwm.sh
sudo sed -i 's_^\(exec /bin/sh /etc/X11/Xsession\)$_#\1_' /etc/xrdp/startwm.sh
echo 'exec /bin/startlxde' | sudo tee --append /etc/xrdp/startwm.sh

# seadistame XRDP automaatselt startima
echo "${USER} ALL=(root) /usr/sbin/service" | sudo tee --append /etc/sudoers
echo "sudo /usr/sbin/service xrdp start" >> ~/.profile