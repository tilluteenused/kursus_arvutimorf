# Windows10 + Ubuntu 22.04@WSL

**MÄRKUS 2023.01.10 Alates 2022 aasta lõpust on graafilise kasutajaliidese tugi W10 WSLi lisatud, st graafilist
kasutajaliidest kasutavad programmid (Gimp, Code, Chrome jne) töötavad "out of box". Seega pigem võiks töötada 
W11 juhend.**

**MÄRKUS 2023.01.10 Kui tahate kasutada mõnda Linuxi graafilise kasutajaliidesega aknahaldurit XFCE, LXD vms, 
siis tuleb ```RDP-service```-i ja linuxi aknahalduri installimist käsitlev osa siiski läbi teha.
Ma ise olen seda proovinud ainult sellise WSLi korral
mis ei toetanud graafilis kasutajaliidest.**

## Plussid & miinused

Plussid:

* Võite julgelt eksperimenteerida virtuaalarvutiga, 
installida peale maha igusugust tarkvara, eksperimenteerida erinevate seadistusega jms
riskeerimata oma "päris" arvuti vussi keeramisega.
* Kui virtuaalarvutit (või seal olevat tarkvara) enam vaja ei lähe,
on seda lihtne kustutada.
* Teie "pärisarvuti" ja virtuaalarvuti opsüsteemid, programmid ja failid on samaegselt kasutatavad.
* Virtualboxist tiba kiirem.

Miinused:

* Teie arvutis peab olema "piisavalt" mälu ja kõvaketast päris- ja virtuaalarvuti tarvis.
* Virtualboxist vähem võimalusi seadistamiseks, virtuaalmasinate salvestamiseks jms. Või vähemalt keerulisem.
* Windows 10 korral (erinevalt Windows 11st) graafilise kasutajaliidesega programmide kasutamiseks tuleb täiendavalt pusida.
(Linuxis **_ei pea_** kasutama ainult terminaliakent koos ```nano```-laadse tekstiredaktoriga.)


## Minimaalne keskkond kasutamiseks

### Eeltöö Windowsis 

* Windowsi seadistamine
    * Uuenda Windowsi tarkvara ja **_taaskäivita_** arvuti.
    * WSLi lubamine:
        * Windowsi otsingulahtrisse: ```windowsi funktsioonide sisse- või väljalülitamine``` ja 
        pane linnukesed nende valikute ette (siin ja edaspidi Windowsi otsingulahtrisse kirjutatavad märksõnad
        on eestindatud Windowsi järgi antud).
            * [V] Virtuaalplatvorm
            * [V] Windowsi alamsüsteem Linuxile
    * **_Taaskäivita arvuti_**
    * **_Kontrolli, et ei unustanud arvutit taaskäivitada_**

* Tarkvara installeerimine
    * Ubuntu installeerimine
        * ```microsoft store``` (otsingusse: ```Ubuntu 22.04```). Installeerimise käigus küsib Ubuntu, 
        millist kasutajanime ja parooli tahad Linuxis kasutada. Võid valida endale meelepärase
        kasutajanime ja parooli. Seda parooli küsitakse iga kord kui on vaja ```sudo``` käsu
        abil Linuxis administraatori õigusi saada.
    * Integreeritud arenduskeskkonna installeerimine Windowsi: https://code.visualstudio.com/download

### Eeltöö wsl-ubuntu-terminaliaknas

* Käivita Ubuntu wsl-terminaliaken (Windowsi otsingusse: ```ubuntu 22.04```)

* Uuenda Ubuntu tarkvara (seda peaks vähemalt kord kuus tegema)

```commandline
sudo apt update && sudo apt -y dist-upgrade
```

* Installi HFSTga seotud tarkvara. Igaks juhuks koos python'i värgiga, äkki läheb hiljem vaja. 
Kui see on korra tehtud, siis nii ongi, rohkem pole vaja. Kõigi programmide ja tarkvara uuendamine toimub automaagiliselt
igakuise ```sudo apt update && sudo apt -y dist-upgrade``` käsu abil.

**_NB! Alljärgnev käsk töötab, kui installisid Ubuntu 22.04. Kui installisid ```Ubuntu 20.04``` versiooni või lihtsalt ```Ubuntu```
(see installib hetkel samuti ```Ubuntu 20.04``` versiooni), siis paketil ```python3-hfst``` on teine nimi._**

```cmdline
sudo apt install -y hfst python3 python3-pip python3-hfst
```

## Ubuntu virtuaalrvuti käivitamine

Käivita Ubuntu wsl-terminaliaken (Windowsi otsingusse: ```ubuntu 22.04```)
Sellest piisab arvutimorfi kursusega alustamiseks.

## Kui väga tahta võib lisada graafilise kasutajaliidese

**MÄRKUS 2023.01.10 Kui tahate kasutada mõnda Linuxi graafilise kasutajaliidesega aknahaldurit XFCE, LXD vms, 
siis tuleb alljärgnev siiski läbi teha. Ma ise olen seda proovinud ainult sellise WSLi korral
mis ei toetanud graafilis kasutajaliidest.**

Selliselt toimeta ainult W10 puhul. W11 WSL'i on graafilise liidese võimekus juba algselt integreeritud.
Sellepärast minu isiklik _teine_ eelistus olekski W10 pealt w11 peale üle minna. Minu _esimene_ eelistus oleks
W10 pealt Linuxi peale üle minna :)
Võib-olla MS lisab graafilise kasutajaliidese võimekuse kunagi ka W10 WSL'i jaoks. 
Praegusel hetkel googeldades tundub, et kõik töötavad lahendused
taanduvad Linuxisse ```RDP-service```'i või alternatiivi installimisele.

* Installi versioonihaldustarkvara ```git```
```commandline
sudo apt install -y git
```

* Tõmba GITLABist need abimaterjalid oma arvuti kataloogi ```~/git/kursus_arvutimorf.gitlab```

```commandline
mkdir ~/git; cd ~/git
git clone https://gitlab.com/tilluteenused/kursus_arvutimorf.git kursus_arvutimorf.gitlab

cd kursus_arvutimorf.gitlab/wsl/
```

* Installi graafilise kasutajaliidese kasutamiseks vajaliku tarkvara
```commandline
./graafiline-liides.sh
```

* Installi MS Code-IDE (õigete pluginatega teeb mis vaja, hea kompromiss funktsionaalsuse ja kasutamismugavuse vahel).

```cmdline
./ide_code.sh
```

Nüüd on meil _kaks_ MS Code-IDEt. Üks töötab Windowsis, teine Linuxis. Tasub hetke mõelda, 
mis seal vahet on ja mis praktilised järeldused sellest tulenevad. Et vältida hilisemaid üllatusi.

## rdp-wsl-ubuntu-lxde aknas

* Kõigepealt käivita Ubuntu wsl-terminaliaken (Windowsi otsingusse: ```ubuntu 22.04```) ja siis
kaugtöölaua ühendus (Windowsi otsingusse: ```kaugtöölaua ühendus```, käivita ```localhost:3390```)
* Edasi terminaliaknaks ```UXTerm``` ja ```code``` mille iganes redigeerimiseks.  


## Mõned võib-olla kasulikud lingid

* https://learn.microsoft.com/en-us/windows/wsl/install
* https://learn.microsoft.com/en-us/windows/wsl/tutorials/gui-apps
* https://www.how2shout.com/how-to/how-to-install-ubuntu-22-04-on-windows-11-or-10-wsl.html
* https://stackoverflow.com/questions/61860208/wsl-2-run-graphical-linux-desktop-applications-from-windows-10-bash-shell-erro
