#!/bin/bash

lisa()
{
    FAILID=$(git status -s | grep "^?? " | sed 's/^?? //g' | tr '\n' ' ')
    if [ ! -z "${FAILID}" ]
    then
        echo -e "\n\nFailid:\t${FAILID// /\\n\\t}"
        echo -ne 'Lisa GITti [y/n] '
        read VASTUS
        if [ "${VASTUS}" = "y" ]
        then
            git add ${FAILID}
            git commit -m "'lisatud: ${FAILID}'"
            git push
        fi
    fi
}

muuda()
{
    FAILID=$(git status -s | grep "^ M " | sed 's/^ M //g' | tr '\n' ' ')
    if [ ! -z "${FAILID}" ]
    then
        echo -e "\n\nFailid:\t${FAILID// /\\n\\t}"
        echo -ne 'Muuda GITis [y/n] '
        read VASTUS
        if [ "${VASTUS}" = "y" ]
        then    
            git add ${FAILID}
            git commit -m "'muudetud: ${FAILID}'"
            git push
        fi
    fi
}

kustuta()
{
    FAILID=$(git status -s | grep "^D " | sed 's/^D //g' | tr '\n' ' ')
    if [ ! -z "${FAILID}" ]
    then
        echo -e "\n\nFailid:\t${FAILID// /\\n\\t}"
        echo -ne 'Kustuta GITist [y/n] '
        read VASTUS
        if [ "${VASTUS}" = "y" ]
        then    
            git add ${FAILID}
            git commit -m "'kustutatud: ${FAILID}'"
            git push
        fi
    fi
    FAILID=$(git status -s | grep "^ D " | sed 's/^ D //g' | tr '\n' ' ')
    if [ ! -z "${FAILID}" ]
    then
        echo -e "\n\nFailid:\t${FAILID// /\\n\\t}"
        echo -ne 'Kustuta GITist [y/n] '
        read VASTUS
        if [ "${VASTUS}" = "y" ]
        then    
            git add ${FAILID}
            git commit -m "'kustutatud: ${FAILID}'"
            git push
        fi
    fi
}

nimetaymber()
{
    FAILID=$(git status -s | grep "^R " | sed 's/^R //g' | tr '\n' ' ')
    if [ ! -z "${FAILID}" ]
    then
        echo -e "\n\nFailid:\t${FAILID// /\\n\\t}"
        echo -ne 'Nimeta ümber GIT [y/n] '
        read VASTUS
        if [ "${VASTUS}" = "y" ]
        then    
            git add ${FAILID}
            git commit -m "'ümbernimetatud: ${FAILID}'"
            git push
        fi
    fi
}

git pull
lisa
muuda
kustuta
nimetaymber

echo -e '\n\n------------\n'
git status -s

exit

X          Y     Meaning
-------------------------------------------------
         [AMD]   not updated
M        [ MTD]  updated in index
T        [ MTD]  type changed in index
A        [ MTD]  added to index
D                deleted from index
R        [ MTD]  renamed in index
C        [ MTD]  copied in index
[MTARC]          index and work tree matches
[ MTARC]    M    work tree changed since index
[ MTARC]    T    type changed in work tree since index
[ MTARC]    D    deleted in work tree
            R    renamed in work tree
            C    copied in work tree
-------------------------------------------------
D           D    unmerged, both deleted
A           U    unmerged, added by us
U           D    unmerged, deleted by them
U           A    unmerged, added by them
D           U    unmerged, deleted by us
A           A    unmerged, both added
U           U    unmerged, both modified
-------------------------------------------------
?           ?    untracked
!           !    ignored
-------------------------------------------------

