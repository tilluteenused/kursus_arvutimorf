#!/bin/env python3

import sys

def graafinda():    
    """ Teisenda hfst-fst2txt väljund dot'i sisendiks

    Loeb std-sisendit ja kirjutab std-väljundisse

    Funktsioonidele/klassidele lisa dokumentatsiooniblokid (poolautomaatselt) code'i laiendiga njpwerner.autodocstring.
    Kasuta kiirklahvi Ctrl+Shift+2
    """       
    print('digraph G { rankdir="LR"')
    print('node [fontname="Tahoma",shape=circle,fontsize=14,fixedsize=true,fillcolor="grey",style=filled]')
    print('edge [fontname="FreeMono",fontsize=14]')
    for line in sys.stdin.readlines():
        line = line.strip().replace('@0@', 'ε')
        row = line.split('\t')
        if len(row) >= 4:
            print(f'{row[0]} [label="{row[0]}"];')
            print(f'{row[0]} -> {row[1]} [label="{row[2]}:{row[3]}"];')
        elif len(row) == 2: # Final state
            print(f'{row[0]} [label="{row[0]}",shape=doublecircle];')
    print('}')

if __name__ == '__main__':
    """ Teisenda hfst-fst2txt väljund dot'i sisendiks

    Kasutamine:
    cat näide2.fst | hfst-fst2txt  | ./att2dot.py | dot -Tpng -o näide2.png
    """    
    # import argparse jne...
    graafinda()
