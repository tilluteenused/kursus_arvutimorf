# GITLAB

## Konfimine

### SSH-võtme paikasättimine

1. Mine [gitlab.cs.ut.ee veebilehele](gitlab.cs.ut.ee) ja logi sisse.

1. Kliki akna ülaservas sinise riba parempoolses servas oleval ikoonil. Avanenud 
rippmenüüst vali ```Preferences```

    <img width=55% src="gitlab-cs-1.png">

1. Terminaliaknas  

    ```cmdline
    $ cd ~/.ssh
    $ cat authorized_keys 
    # siia tulevad sinu avalikud ssh-võtmed
    ```

1. Kopeeri clipboard'i oma avalik ssh-võti. Algab ```ssh-rsa AAAAB3NzaC1yc2EAAAADAQ``` ja lõppeb ```d15qGjlt55Q5p4gOBbDAenEX8Hr kmuis@eliza```

1. Avanenud aknas lahtrisse ```Key``` kopeeri clipboard'ist ssh-võti, pane talle ```Title``` lahtris (misiganes) nimi ja kliki ```Add key```

    <img width=55% src="gitlab-cs-2.png">

1. Sama akna alumises osas ```Your SSH keys``` järel peaks nüüd olema sinu lisatud ssh-võti.

### Järgmised soovitatavad konfigureerimise sammud

* Konfi kahetasemeline autentimine (hädapärast saab (vist) ilma ka)

    1. Tõmba moblasse äpipoest ```Google Authenticator```.
    2. Konfi gitlabi lehel  kahtasemeline autentimine (sellest siis kui selleks läheb).


## Kasutamine

### Repost failide endale tõmbamine

1. Mine [gitlab.cs.ut.ee veebilehele](gitlab.cs.ut.ee) ja logi sisse.

1. Kliki ```krp-niit``` peal.

1. Kliki ```Clone``` nuppu ja kopeeri clipboardi ```Clone with SSH``` alt tekst.
Midagi sellist võiks seal olla: ```git@gitlab.cs.ut.ee:kmuis/krp_niit.git```

1. Ava terminaliaken ja sisesta:

    ```cmdline
    mkdir ~/git
    cd ~/git
    git clone --depth 1 git@gitlab.cs.ut.ee:kmuis/krp_niit.git krp_niit_gitlabcs
    ```

1. See ```git@gitlab...krp_niit.git``` osa on see mis punktis 3. kopeerisid.

1. Põhimõtteliselt võiksid kloonimist alustada kohe punktist 4.  Pikk jutt on sellepärast, et ma pole päris kindel, kas sa seal oled ```kmuis```.
