# Mõned arvutimorfi kursusega seotud mõtted [2022]

## Seadistamine

### Windows11+WSL2+Ubuntu22.04
[Juhend: Windows 11](https://gitlab.com/tilluteenused/kursus_arvutimorf/-/blob/main/wsl/README-W11.md)

### Windows10+WSL+Ubuntu22.04
[Juhend: Windows 10](https://gitlab.com/tilluteenused/kursus_arvutimorf/-/blob/main/wsl/README-W10.md)

### Windows+Virtualbox+Ubuntu22.04
[Juhend](https://gitlab.com/tilluteenused/kursus_arvutimorf/-/tree/main/virtualbox)
Kui W11+WSL2 ei toimi või tahta Linuxit GNOME-i aknahalduriga või tahta saada aimu kuidas "päris" Linux võiks olla,
siis see variant.

### Windows+X2Go+Ubuntu22.04
[Juhend](https://gitlab.com/tilluteenused/kursus_arvutimorf/-/blob/main/x2go/README.md)
Kui läpaks WSL2-hte või Virtualbox-i välja ei vea, võiks kasutada seda.

### Dualboot: Windows ja Ubuntu 22.04

[Juhend](https://gitlab.com/tilluteenused/kursus_arvutimorf/-/tree/main/dualboot)
Kui läpaks WSL2-hte või Virtualbox-i välja ei vea, aga kõvaketas on piisavalt suur, siis see võib ka variant olla.

## Kuidas valida töökeskkond

Lühidalt iga keskkonna plussidest ja miinustest vaata vastava keskkonna juhendist. 

Kaaludes oma vaatevinklist erinevate keskkondade plusse ja miinuseid võivad sul olla keskkonna valiku
suhtes teistsugused eelistused (ja tõenäoliselt ongi).

* Kui arvutis on alla 16G op mälu:

  * [Windows+X2Go+Ubuntu22.04](https://gitlab.com/tilluteenused/kursus_arvutimorf/-/blob/main/x2go/README.md).
  * [Dualboot: Windows ja Ubuntu 22.04](https://gitlab.com/tilluteenused/kursus_arvutimorf/-/tree/main/dualboot). See variant on pigem IT-gurule.
  
* Kui arvutis on 16G või rohkem op mälu:

  * Arvutis on Windows 11. Soovitaks proovida sellises järjekorras (eelistatum variant eespool):
    * [Windows+WSL2+Ubuntu22.04](https://gitlab.com/tilluteenused/kursus_arvutimorf/-/blob/main/wsl/README-W11.md)
    * [Windows+Virtualbox+Ubuntu22.04](https://gitlab.com/tilluteenused/kursus_arvutimorf/-/tree/main/virtualbox)
    * [Windows+X2Go+Ubuntu22.04](https://gitlab.com/tilluteenused/kursus_arvutimorf/-/blob/main/x2go/README.md)
    * [Dualboot: Windows ja Ubuntu 22.04](https://gitlab.com/tilluteenused/kursus_arvutimorf/-/tree/main/dualboot)

  * Arvutis on Windows 10. Soovitaks proovida sellises järjekorras (eelistatum variant eespool):
    * [Windows+Virtualbox+Ubuntu22.04](https://gitlab.com/tilluteenused/kursus_arvutimorf/-/tree/main/virtualbox)
    * [Windows+X2Go+Ubuntu22.04](https://gitlab.com/tilluteenused/kursus_arvutimorf/-/blob/main/x2go/README.md)
    * [Windows+WSL+Ubuntu22.04](https://gitlab.com/tilluteenused/kursus_arvutimorf/-/blob/main/wsl/README-W10.md)
    * [Dualboot: Windows ja Ubuntu 22.04](https://gitlab.com/tilluteenused/kursus_arvutimorf/-/tree/main/dualboot)


## Muu värk

* [Töökeskkondade jõudluse võrdlus](https://gitlab.com/tilluteenused/kursus_arvutimorf/-/tree/main/vordlus)
* [Abiks Linuxi käsurea ja skriptidega majandamisel](https://gitlab.com/tilluteenused/kursus_arvutimorf/-/tree/main/bash_guide)
